from subprocess import PIPE, Popen
import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.dates as mdates
import pandas as pd

HEADERS = ["Start_Time", "End_Time", "Time_Diff", "Start_Mem", "End_Mem", "Mem_Diff",
           "Start_Mem_Free", "End_Mem_Free", "Mem_Free_Diff", "Queue_Start", "Queue_End",
           "Queue_Diff", "Periodic_Task_Start", "Periodic_Task_End", "Periodic_Task_Diff",
           "Refresh_Sim_Num_Start", "Refresh_Sim_Num_End", "Refresh_Sims_Diff",
           "Top_Mem_Difference"]

def main() -> None:
    mem_leak_dir = os.path.join("/", "media", "cluster", "Data", "WebApps", "development",
                                "Jago", "mem_leak")
    df = read_from_csv(os.path.join(mem_leak_dir, "refresh_simulation_stats.csv"))
    plot_memory(df, os.path.join(mem_leak_dir, "memory_usage_plot.png"))
    plot_time(df, os.path.join(mem_leak_dir, "time_taken_plot.png"))

def read_docker_logs() -> pd.DataFrame:
    cmd = ["docker", "logs", "jago_prod-celery_cpu-1", *sys.argv[1:]]
    with Popen(cmd, stdout=PIPE, stderr=PIPE, text=True) as proc:
        logs = proc.stderr.read().split("System Statistics: ")[1::2]
    transposed_logs = zip(*[log.split(',')[:len(HEADERS)] for log in logs])
    data = [pd.Series(l, name=HEADERS[i]) for i, l in
            enumerate(transposed_logs)]
    return pd.concat(data, axis=1)

def read_from_csv(path: str, date_fmt: str="%Y-%m-%d %H:%M:%S.%f") -> pd.DataFrame:
    df = pd.read_csv(path)
    for col in ("Start_Time", "End_Time"):
        df[col] = pd.to_datetime(df[col], format=date_fmt)
    df["Time_Diff"] = pd.to_timedelta(df["Time_Diff"])
    return df

def convert_types(df: pd.DataFrame) -> None:
    for name in HEADERS:
        if name in ["Start_Time", "End_Time"]:
            df[name] = pd.to_datetime(df[name], format="%Y-%m-%d-%H-%M-%S")
        elif name == "Time_Diff":
            df[name] = pd.to_numeric(df[name])
            df[name] = pd.to_timedelta(df[name], unit='s')
        elif name == "Top_Mem_Difference":
            continue
        else:
            df[name] = pd.to_numeric(df[name])

def plot_memory(df: pd.DataFrame, path: str) -> None:
    x = df["Start_Time"]
    MiB = 1 / 1024

    mem_fig, (mem_ax, mem_diff_ax) = plt.subplots(nrows=2, ncols=1, sharex=True)
    mem_ax.plot(x, df["Start_Mem"] * MiB, label="Start Memory Usage")
    mem_ax.plot(x, df["End_Mem"] * MiB, label="End Memory Usage")
    mem_ax.plot(x, df["Start_Mem_Free"] * MiB, label="Start Memory Free")
    mem_ax.legend()
    mem_diff_ax.plot(x, df["Mem_Diff"] * MiB,
                     label=f"Memory Usage Difference\n"
                           f"Median = {df['Mem_Diff'].median() * MiB}\n"
                           f"Mean = {df['Mem_Diff'].mean() * MiB}\n"
                           f"STD = {df['Mem_Diff'].std() * MiB}")
    mem_diff_ax.legend()
    mem_diff_ax.xaxis.set_major_locator(mdates.HourLocator())
    mem_diff_ax.tick_params(axis="x", rotation=45)
    mem_diff_ax.set_xlabel("Time")
    mem_ax.set_ylabel("Memory Usage / MiB")
    mem_diff_ax.set_ylabel("Memory Usage Difference / MiB")
    mem_fig.tight_layout

    mem_fig.savefig(path, bbox_inches="tight")

def plot_time(df: pd.DataFrame, path: str) -> None:
    seconds = df['Time_Diff'].dt.total_seconds()
    time_fig, (ax, hist_ax) = plt.subplots(nrows=1, ncols=2, sharey=True)
    ax.tick_params(axis="x", rotation=45)
    ax.plot(df["Start_Time"], seconds)
    ax.set_xlabel("Time")
    ax.set_ylabel("Time taken by task / s")
    nbins = int(np.sqrt(len(seconds)))
    hist_ax.hist(seconds, orientation='horizontal', bins=nbins,
                 label=f"Median = {seconds.median()}\n"
                       f"Mean = {seconds.mean()}\n"
                       f"Std = {seconds.std()}\n"
                       f"Max = {seconds.max()}\n"
                       f"Number of bins = {nbins}")
    hist_ax.set_xlabel("Frequency")
    hist_ax.legend()

    time_fig.tight_layout

    time_fig.savefig(path, bbox_inches="tight")

if __name__ == "__main__":
    main()