import csv
import json
import logging
import os
from typing import TypeVar
from datetime import datetime
import redis



class LogSystemStats:
    def __init__(self,
                 logger: logging.Logger=None,
                 csv: str=None,
                 time_fmt: str="%Y-%m-%d %H:%M:%S"):
        self.logger = logger
        self.csv = csv
        self.time_fmt = time_fmt

    def __enter__(self):
        self.start_dict = self._get_system_stats()

    def __exit__(self):
        self.end_dict = self._get_system_stats()
        self.system_stats = self._get_total_system_stats()
        if self.csv:
            self.save_stats()
        if self.logger:
            self.log_stats()

    def _get_total_system_stats(self) -> dict:
        system_stats = {
            "Start_Time": self.start_dict["Time"],
            "End_Time": self.end_dict["Time"],
            "Start_Mem": self.start_dict["Used_Mem"],
            "End_Mem": self.end_dict["Used_Mem"],
            "Start_Mem_Free": self.start_dict["Free_Mem"],
            "End_Mem_Free": self.end_dict["Free_Mem"],
            "Queue_Start": self.start_dict["Queue_Num"],
            "Queue_End": self.end_dict["Queue_Num"],
            "Periodic_Task_Start": self.start_dict["Periodic_Task_Num"],
            "Periodic_Task_End": self.end_dict["Periodic_Task_Num"],
            "Refresh_Sim_Num_Start": self.start_dict["Refresh_Sims_Num"],
            "Refresh_Sim_Num_End": self.end_dict["Refresh_Sims_Num"],
        }
        system_stats["Time_Diff"] = system_stats["End_Time"] - system_stats["Start_Time"]
        system_stats["Mem_Diff"] = system_stats["End_Mem"] - system_stats["Start_Mem"]
        system_stats["Queue_Diff"] = system_stats["Queue_End"] - system_stats["Queue_Start"]
        system_stats["Periodic_Task_Diff"] = system_stats["Periodic_Task_End"] \
                                             - system_stats["Periodic_Task_Start"]
        system_stats["Refresh_Sims_Num_Diff"] = system_stats["Refresh_Sims_Num_End"] \
                                                - system_stats["Refresh_Sims_Num_Start"]
        return system_stats

    def _get_system_stats(self) -> dict:
        with redis.Redis('redis', db=0) as r_conn:
            queue_num = 0
            periodic_task_num = 0
            refresh_sim_num = 0
            for item in r_conn.lrange('celery', 0, -1):
                queue_num += 1
                task_headers = json.loads(item)["headers"]
                task_name = task_headers["task"]
                if 'periodic_tasks' in task_name:
                    periodic_task_num += 1
                    if 'refresh_simulation_stats' in task_name:
                        refresh_sim_num += 1
        used_m, free_m = map(int, os.popen('free -t -m').readlines()[-1].split()[2:])
        return {"Time": datetime.now(),
                "Used_Mem": used_m,
                "Free_Mem": free_m,
                "Queue_Num": queue_num,
                "Periodic_Task_Num": periodic_task_num,
                "Refresh_Sims_Num": refresh_sim_num,
        }

    def save_stats(self) -> None:
        with open(self.csv, 'a', newline='') as f:
            w = csv.writer(f)
            if os.stat(self.csv).st_size == 0:
                w.writerow(self.system_stats.keys())
            w.writerow(self.system_stats.values())

    def stats_to_strings(self) -> dict:
        stats_as_strings = {}
        for key in self.system_stats.keys():
            if key in ["Start_Time", "End_Time"]:
                stats_as_strings[key] = self.system_stats[key].strftime(self.time_fmt)
            elif key == "Time_Diff":
                stats_as_strings[key] = str(self.system_stats[key].total_seconds())
            else:
                stats_as_strings[key] = str(self.system_stats[key])
        return stats_as_strings

    def log_stats(self):
        self.logger.info("System Statistics: %s", ','.join(self.stats_to_strings().values()))